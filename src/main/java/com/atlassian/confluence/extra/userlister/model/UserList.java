package com.atlassian.confluence.extra.userlister.model;

import com.atlassian.confluence.user.UserAccessor;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class UserList {
    public static final String ALL_GROUP_NAME = "*";

    private final String groupName;
    private final List<String> usernames;
    private final Set<String> loggedInUsernames;
    private final UserAccessor userAccessor;
    private final boolean useSpecificGroupName;

    private List<ListedUser> users;

    public UserList(String groupName, UserAccessor userAccessor, List<String> usernames, Set<String> loggedInUsernames) {
        this.groupName = groupName;
        this.userAccessor = userAccessor;
        this.usernames = usernames;
        this.loggedInUsernames = loggedInUsernames;
        this.useSpecificGroupName = !groupName.equals(ALL_GROUP_NAME);
    }

    public String getGroup() {
        return groupName;
    }

    public List<ListedUser> getUsers() {
        if (users == null) {
            loadUsers();
        }
        return Collections.unmodifiableList(users);
    }

    //used by userlistermacro.vm
    @SuppressWarnings("unused")
    public boolean isUseSpecificGroupName() {
        return useSpecificGroupName;
    }

    private void loadUsers() {
        users = usernames.stream().map(userAccessor::getUserByName)
                .filter(Objects::nonNull)  // If it's an anonymous user, then we can't do anything (CONF-5821)
                .filter(user -> !userAccessor.isDeactivated(user))  // we don't know group users are active, so check.
                .map(user -> new ListedUser(user, loggedInUsernames.contains(user.getName())))
                .collect(Collectors.toList());

        Collections.sort(users,
                Comparator.comparing(
                        (ListedUser user) -> StringUtils.isBlank(user.getFullName()) ? user.getName() : user.getFullName(),
                        String.CASE_INSENSITIVE_ORDER));
    }
}
