package com.atlassian.confluence.extra.userlister.model;

import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.user.UserAccessor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserListTestCase {

    private static final String ADMIN_USER_NAME = "admin";
    private static final String JDOE_USER_NAME = "jdoe";
    private static final String CONFLUENCE_USERS_GROUP = "confluence-users";

    @Mock
    private UserAccessor userAccessor;

    @Before
    public void setUp() {
        when(userAccessor.getUserByName(anyString()))
                .thenAnswer(invocationOnMock -> new ConfluenceUserImpl(invocationOnMock.getArguments()[0].toString(), null, null));
    }

    @Test
    public void testUsersListedWithCorrectOrderAndStatus() {
        UserList userList = new UserList(CONFLUENCE_USERS_GROUP, userAccessor, asList(JDOE_USER_NAME, ADMIN_USER_NAME), new HashSet<>(singletonList(ADMIN_USER_NAME)));
        List<ListedUser> users = userList.getUsers();

        assertEquals(2, users.size());
        assertEquals(ADMIN_USER_NAME, users.get(0).getName());
        assertTrue(users.get(0).isLoggedIn());
        assertEquals(JDOE_USER_NAME, users.get(1).getName());
        assertFalse(users.get(1).isLoggedIn());
    }
}
