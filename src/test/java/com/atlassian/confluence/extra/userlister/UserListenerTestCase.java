package com.atlassian.confluence.extra.userlister;

import com.atlassian.confluence.event.events.security.LoginEvent;
import com.atlassian.confluence.event.events.security.LogoutEvent;
import com.atlassian.user.User;
import com.atlassian.user.impl.DefaultUser;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserListenerTestCase {

    @Mock
    private UserListManager userListManager;
    @InjectMocks
    private UserListener userListener;

    @Test
    public void testHandleUserLoginEvent() {
        final User userWhoLoggedIn = new DefaultUser("admin");
        final String _sessionId = RandomStringUtils.randomAlphanumeric(32);
        final LoginEvent loginEvent = new LoginEvent(this, userWhoLoggedIn.getName(), _sessionId, "localhost", "127.0.0.1", null);

        userListener.handleLoginEvent(loginEvent);
        verify(userListManager).registerLoggedInUser(userWhoLoggedIn.getName(), _sessionId);
    }

    @Test
    public void testHandleUserLoggedOutEvent() {
        final User userWhoLoggedOut = new DefaultUser("admin");
        final String _sessionId = RandomStringUtils.randomAlphanumeric(32);
        final LogoutEvent logoutEvent = new LogoutEvent(this, userWhoLoggedOut.getName(), _sessionId, "localhost", "127.0.0.1");

        userListener.handleLogoutEvent(logoutEvent);
        verify(userListManager).unregisterLoggedInUser(userWhoLoggedOut.getName(), _sessionId);
    }

}
